import itertools
import logging
import os
import re
import time
import yaml
import pathlib
import subprocess
import dns.resolver
import smtplib
from enum import Enum
from datetime import datetime
from collections import defaultdict
from collections import OrderedDict
from email.mime.text import MIMEText
from email.header import Header
from email.charset import Charset
import mimetypes
import lxml.etree as etree
from lxml.builder import E

class RepositoryType(Enum):
    "Enum type - indicates the type of repository we are working with"
    Unknown = 0
    Project = 1
    Wiki = 2
    Design = 3
    Snippets = 4

class ChangeType(Enum):
    "Enum type - indicates the type of change to a ref"
    Update = 1
    Create = 2
    Delete = 3
    Forced = 4

class RefType:
    "Enum type - indicates the type of ref in the repository"
    Branch = "branch"
    WorkBranch = "work-branch"
    Tag = "tag"
    Backup = "backup"
    Notes = "notes"
    MergeRequest = "merge-requests"
    Internal = "internal"
    Unknown = 0

class NotificationAllowed(Enum):
    "Enum type - determines what forms of notification are allowed"
    Publicly = 1
    Privately = 2
    Never = 3

class Repository:
    "Represents our repository and the pending incoming changes we will be checking"

    def __init__(self, repository_root, repository_disk_path, pusher_username):
        # Save our operating parameters
        self.pusher_username = pusher_username
        self.disk_path = pathlib.Path( os.path.realpath( repository_disk_path ) )

        # Perform some initialization (including initial defaults for later)
        self.changesets = {}
        self.push_options = []
        self.repo_type = RepositoryType.Project

        # Now we can determine the virtual/user presented path to the repository
        # As a safe baseline that should work anywhere, we chop the repository root off the full disk path
        repository_root = pathlib.Path( repository_root )
        self.path = str( self.disk_path.relative_to(repository_root).with_suffix("") )

        # If we are running under Gitlab however, this path likely does not mean much as virtually all installations of it use hashed storage now
        # Therefore we need to read some Gitaly environment variables to find out what the user visible path is...
        if 'GL_REPOSITORY' in os.environ and 'GL_PROJECT_PATH' in os.environ:
            # Retrieve the human usable path from the Gitaly environment variable
            self.path        = os.environ['GL_PROJECT_PATH']

            # While we are here, we can also determine what type of repository we are dealing with
            # Gitaly represents these as the type followed by a dash, then the project ID number the repository belongs to (eg: project-1234)
            # We ignore the project ID number as it isn't relevant to us at this stage
            if os.environ['GL_REPOSITORY'].startswith('project-'):
                self.repo_type = RepositoryType.Project
            elif os.environ['GL_REPOSITORY'].startswith('wiki-'):
                self.repo_type = RepositoryType.Wiki
            elif os.environ['GL_REPOSITORY'].startswith('design-'):
                self.repo_type = RepositoryType.Design
            elif os.environ['GL_REPOSITORY'].startswith('snippet-'):
                self.repo_type = RepositoryType.Snippets
            else:
                self.repo_type = RepositoryType.Unknown

        # Do we have any git push options that we need to retrieve?
        if 'GIT_PUSH_OPTION_COUNT' in os.environ:
            # Pull out all of the options we have been given
            position = 0
            for position in range(int(os.environ['GIT_PUSH_OPTION_COUNT'])):
                # Construct the environment variable name
                variable = 'GIT_PUSH_OPTION_{0}'.format( position )
                # Retrieve it from our environment and save it
                self.push_options.append( os.environ[variable] )
                # Increment our position and move on to the next one
                position += 1

        # We're now ready to go!
        return

    # Register a new changeset (being a reference and the change taking place to it) with us
    def add_changeset(self, ref, old_sha1, new_sha1):
        # Create the new changeset, letting it know it belongs to us
        new_changeset = Changeset( self, ref, old_sha1, new_sha1 )
        # Register it with our list of known changesets
        self.changesets[ ref ] = new_changeset
        # Return the new changeset in case our caller needs it
        return new_changeset

class Changeset:
    "Represents a specific Git reference that is being changed in a repository, as well as all of the constituent new commits being introduced"

    # Enums we will need later...
    EmptyRef = "0000000000000000000000000000000000000000"

    def __init__(self, repository, reference, old_sha1, new_sha1):
        # Save our parameters for later
        self.repository = repository
        self.ref_path = reference
        self.old_sha1 = old_sha1
        self.new_sha1 = new_sha1

        # Perform some initialization for later
        self.commits = {}

        # First off, we need to know what type of reference we are dealing with here
        # Is this a branch, tag, etc? (being the first part of refs/type/name)
        if re.match("^refs/heads/work/(.+)$", self.ref_path):
            self.ref_type = RefType.WorkBranch
        elif re.match("^refs/heads/(.+)$", self.ref_path):
            self.ref_type = RefType.Branch
        elif re.match("^refs/tags/(.+)$", self.ref_path):
            self.ref_type = RefType.Tag
        elif re.match("^refs/backups/(.+)$", self.ref_path):
            self.ref_type = RefType.Backup
        elif re.match("^refs/notes/(.+)$", self.ref_path):
            self.ref_type = RefType.Notes
        elif re.match("^refs/merge-requests/(.+)$", self.ref_path):
            self.ref_type = RefType.MergeRequest
        elif re.match("^refs/keep-around/(.+)$", self.ref_path):
            self.ref_type = RefType.Internal
        elif re.match("^refs/pipelines/(.+)$", self.ref_path):
            self.ref_type = RefType.Internal
        elif re.match("^refs/environments/(.+)$", self.ref_path):
            self.ref_type = RefType.Internal
        else:
            self.ref_type = RefType.Unknown

        # Now we can determine the name of this reference
        ref_name_match = re.match("^refs/(.+?)/(.+)$", self.ref_path)
        self.ref_name = ref_name_match.group(2)

        # Now we should determine the type of change we're experiencing
        # First, determine the merge base, to detect if we are experiencing a force or normal push....
        if( self.old_sha1 != self.EmptyRef and self.new_sha1 != self.EmptyRef ):
            merge_base = read_command('git', 'merge-base', self.old_sha1, self.new_sha1)

        # Then determine the type of change that is taking place
        if self.old_sha1 == self.EmptyRef and self.new_sha1 != self.EmptyRef:
            self.change_type = ChangeType.Create
        elif self.new_sha1 == self.EmptyRef:
            self.change_type = ChangeType.Delete
        elif self.old_sha1 != merge_base:
            self.change_type = ChangeType.Forced
        else:
            self.change_type = ChangeType.Update

        # Now that we know all of that, we can determine the type of the top most commit that is being pushed
        # While this may seem oddly specific, it is helpful for determining the type of tag that is being pushed
        if self.change_type == ChangeType.Delete:
            self.commit_type = "commit"
        else:
            command = ["git", "cat-file", "-t", self.new_sha1]
            process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            self.commit_type = process.stdout.readline().strip().decode('utf-8')

        # Finally we now retrieve all of the information on the new commits that are being introduced
        self.__build_commits()

    def __build_commits(self):
        # Build the revision span git will use to help build the revision list...
        if self.change_type == ChangeType.Delete:
            return
        elif self.change_type == ChangeType.Create:
            revision_span = self.new_sha1
        else:
            merge_base = read_command('git', 'merge-base', self.new_sha1, self.old_sha1)
            revision_span = f"{merge_base}..{self.new_sha1}"

        # Determine what command we need to use to fetch the list of revisions
        #
        # To start, the merge request refs, along with the keep-around refs used by Gitlab for it's operations are both ignored and not considered to be already in the repository
        # We also ignore work branches, as these are subject to force pushes and are considered to be a work in progress and not yet part of a release
        #
        # This is necessary to ensure hooks fire when these changes are merged into the repository and become part of a release branch
        revisionsCommand = "git for-each-ref --format='%(refname) ^%(objectname)' | grep -E '^refs/(heads|tags|backups)/' | grep -v '^refs/heads/work/' | grep -v '^refs/backups/work-branch-' | cut -d ' ' -f 2 | sort -u | grep -v {old_sha} | git rev-list --reverse --stdin {span}"

        # Now we have the command, actually get the list of revisions we are to be working on!
        command = revisionsCommand.format(old_sha = self.old_sha1, span = revision_span)
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        revisions = process.stdout.readlines()

        # If we have no revisions... don't continue
        if not revisions:
            return

        # Build the git pretty format + regex.
        # This is made up of two interlinked components:
        # - The Git "pretty format" expression which is used by the 'git show' command itself to format it's output
        # - The Python regular expression which will be used by our code to split out all of those elements from the "git show" output
        #
        # All of these elements need to be stored as raw byte data due to the use of control characters such as \x00 and \xff which are poorly handled by UTF-8
        # 
        # The format of the below is as follows:
        # - First element (CH) is the common seperator used to identify the start of this particular bit of information
        # - Second element (%H%n) is the Git pretty format expression used to produce this particular bit of information
        # - Third element ((?P<sha1>.+)\n) is the Python regular expression used to extract the information
        l = (
             (b'CH' , (b'%H%n',   b'(?P<sha1>.+)\n')),
             (b'CP' , (b'%P%n',   b'(?P<parents>.*)\n')),
             (b'AN' , (b'%an%n',  b'(?P<author_name>.*)\n')),
             (b'AE' , (b'%ae%n',  b'(?P<author_email>.*)\n')),
             (b'D'  , (b'%ct%n',  b'(?P<date>.+)\n')),
             (b'CN' , (b'%cn%n',  b'(?P<committer_name>.*)\n')),
             (b'CE' , (b'%ce%n',  b'(?P<committer_email>.*)\n')),
             (b'MSG', (b'%B%xff', b'(?P<message>(.|\n)*)\xff(\n*)(\x00*)(?P<files_changed>(.|\n)*)'))
            )

        pretty_format_data = (b': '.join((outer, inner[0])) for outer, inner in l)
        pretty_format_raw = b'%xfe%xfa%xfc' + b''.join(pretty_format_data)
        pretty_format = pretty_format_raw.decode('utf-8')

        re_format_data = (b': '.join((outer, inner[1])) for outer, inner in l)
        re_format = b'^' + b''.join(re_format_data) + b'$'

        # Extract information about commits....
        # We need to ensure we pass a list of commit hashes to `git show` in order for it to provide us the information on those commits
        command = "git show --stdin --name-status -z -C --pretty=format:'{0}'".format(pretty_format)
        process = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        for sha1 in revisions:
            process.stdin.write(sha1)
        process.stdin.close()
        output = process.stdout.read()

        # Parse time!
        # Each commit is separated from the previous one by \xfe\xfa\xfc in accordance with the %xfe%xfa%xfc pretty format used above to mark the start of each commit
        split_out = output.split(b"\xfe\xfa\xfc")
        # Because we use \xfe\xfa\xfc to mark the start of each commit, we need to remove the empty bit of data we receive at the very beginning
        split_out.remove(b"")
        # Now that the information is ready, start processing
        for commit_data in split_out:
            match = re.match(re_format, commit_data, re.MULTILINE)
            commit = Commit(self, match.groupdict())
            self.commits[ commit.sha1 ] = commit

        # Extract the commit descriptions....
        command = ("xargs", "git", "describe", "--always")
        process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        describeRevs = b'\n'.join( revisions )
        stdout, stderr = process.communicate( input=describeRevs )
        descriptions = stdout.decode('utf-8').split('\n')
        for rev,desc in zip(revisions, descriptions):
            revision_sha1 = rev.strip().decode('utf-8')
            self.commits[ revision_sha1 ].description = desc.strip()

        # Retrieve number of changed lines, and merge in the type of change
        process = get_change_diff( self, ["--numstat", "-z"] )
        output = process.stdout.read()
        for sha1, information in re.findall(b"\xff([0-9a-f]+)\xff(?:\n|\x00|)([^\xff]*)(?:\x00|)", output):
            # Make sure the commit hash is usable
            sha1 = sha1.decode('utf-8')

            # Determine the number of lines changed in each file
            stats = defaultdict(dict)
            file_stats = re.findall(b"([0-9]+|-)\t([0-9]+|-)\t(?:(?=\x00)\x00([^\x00]+)\x00|)([^\x00]+)", information)
            for added, removed, source_file, changed_file in file_stats:
                if source_file:
                    stats[changed_file]["source"] = source_file.decode('utf-8', 'replace')
                stats[changed_file]["added"] = added.decode('utf-8')
                stats[changed_file]["removed"] = removed.decode('utf-8')

            # Parse the way files changed
            status = re.findall(b"\x00?(A|C|D|M|R|T|U|X)(?:(?<=C|R)([0-9]+)\x00([^\x00]+)|)\x00([^\x00]+)?", self.commits[sha1].files_changed)
            for change, similarity, source_file, changed_file in status:
                stats[changed_file]["change"] = change.decode('utf-8')
                if source_file:
                    stats[changed_file]["similarity"] = similarity.decode('utf-8')

            for filename, data in stats.items():
                if "source" in data.keys() and "similarity" not in data.keys():
                    del data["source"]

            # Remove items with invalid data (ie. number of changed lines but no status)
            data = OrderedDict( (filename.decode("utf-8", "replace"), data) for filename, data in sorted(stats.items()) if "change" in data and "added" in data )
            self.commits[sha1].files_changed = data

    # Backup the reference we represent - to ensure a copy is protected for future reference in case of destructive operations
    def backup_ref(self):
        # Back ourselves up!
        backup_ref = "refs/backups/{0}-{1}-{2}".format( self.ref_type, self.ref_name, int(time.time()) )
        command = ("git", "update-ref", backup_ref, self.old_sha1)
        process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        process.wait()

    def commits_with_diff(self):
        # If there are no commits -> nothing for us to provide details on
        if len(self.commits) == 0:
            return

        # Start a list of the commits we have processed, so we know which ones to skip over
        seenCommits = []

        # Initialize some variables that will be used later
        diff = []
        commit = ""

        # We will incrementally yield as we gather up the diffs....
        process = get_change_diff( self, ["-p"] )
        for line in process.stdout:
            # First, do we have a change in the commit we're looking at?
            # If we do, then yield - however only do so if it isn't a commit we have seen before
            commit_change = re.match( b"^\xff(.+)\xff$", line )
            if commit_change and diff and commit not in seenCommits:
                # Pass the information through for the invoker to use
                yield(self.commits[commit], diff)
                # Register this commit as seen
                seenCommits.append( commit )
                # We don't need to reset here as the statement below will do that for us now that this commit has been marked as seen

            # If the commit is changing, and we have a valid diff but the commit has been seen before then make sure everything is reset
            if commit_change and diff and commit in seenCommits:
                # Reset for the next one
                commit = ""
                diff = []

            # If we have a change in commit, then save the hash of the new commit...
            if commit_change:
                commit = commit_change.group(1).decode('utf-8')
                continue

            # Otherwise if we don't have a change in commit, this must be a diff line - so save it!
            diff.append( line.decode('utf-8', 'replace') )

        # Finally, once we've finished processing all lines we might have a final commit to process
        # (as the last one will never experience a commit change at the end)
        # Therefore, force processing if we seem to have a commit waiting for us
        if commit and diff:
            yield(self.commits[commit], diff)

class Commit:

    """Represents a git commit"""

    UrlPattern = "https://commits.kde.org/{0}/{1}"

    def __init__(self, changeset, commit_data):
        self.changeset = changeset
        self._commit_data = commit_data
        self._raw_properties = ["files_changed", "datetime"]
        self.url = Commit.UrlPattern.format( changeset.repository.path, self.sha1 )

        # Convert the date into something usable...
        self._commit_data["datetime"] = datetime.fromtimestamp( float(self._commit_data["date"]) )

    def __getattr__(self, key):
        if key not in self._commit_data:
            raise AttributeError

        value = self._commit_data[key]
        if type(value) is bytes and key not in self._raw_properties:
            return value.decode('utf-8', 'replace')

        return value

    def __setattr__(self, key, value):
        if key not in ['_commit_data', '_raw_properties', 'changeset']:
            self._commit_data[key] = value
        else:
            self.__dict__[key] = value

    def __repr__(self):
        return str(self._commit_data)

class RepositoryMetadataLoader:
    # Store of repositories we know about
    KnownRepos = {}

    # Load the projects from the YAML file
    def loadProjectsFromTree( self, directoryPath ):
        # Start going over the location in question...
        for currentPath, subdirectories, filesInFolder in os.walk( directoryPath, topdown=False, followlinks=False ):
            # Do we have a metadata.yaml file?
            if 'metadata.yaml' not in filesInFolder:
                # We're not interested then....
                continue

            # Now that we know we have something to work with....
            # Lets load the current metadata up
            metadataPath = os.path.join( currentPath, 'metadata.yaml' )
            metadataFile = open( metadataPath, 'r' )
            metadata = yaml.safe_load( metadataFile )

            # Extract the project path...
            projectPath = metadata['repopath']

            # Now that we have everything we need add it to our list of repos
            self.KnownRepos[ projectPath ] = metadata

class CommitAuditor:

    """Performs all audits on commits"""

    # Get ourselves setup
    def __init__(self, logger):
        # Initialize default state - by default we don't have any problems
        self.__failed = False

        # Initialise a list of commits that we should be ignoring (empty by default)
        self.commits_to_ignore = []

        # Save our logger for later use
        self.__logger = logger

    # Convenience helper for declaring a failure
    def log_failure(self, commit, message):
        # Are we supposed to ignore this?
        if commit in self.commits_to_ignore:
            log_message = "Audit exception - Commit {0} - {1}".format(commit, message)
            self.__logger.critical(log_message)
            return

        # Otherwise proceed to fail as normal
        log_message = "Audit failure - Commit {0} - {1}".format(commit, message)
        self.__logger.critical(log_message)
        self.__failed = True

    # Convenience helper for declaring a potential problem (but that isn't a failure)
    def log_warning(self, commit, message):
        log_message = "Audit warning - Commit {0} - {1}".format(commit, message)
        self.__logger.warning(log_message)

    # Allow for the status of the audit (failure = True, passed = False) to be determined
    @property
    def audit_failed(self):
        return self.__failed

    # Audit the new commits in the provided changeset for proper (Unix) end of line characters
    # Files that have a mimetype contained in allowed_mimetypes, or that have a file extension in allowed_extensions will be ignored
    # Additionally, only new files / changes to existing files will be considered
    def audit_eol(self, changeset, allowed_mimetypes, allowed_extensions):
        # Regexes....
        re_commit = re.compile(b"^\xff(.+)\xff$")
        re_filename = re.compile("^diff --(cc |git a\/.+ b\/)(.+)$")
        blocked_eol = re.compile(r"(?:\r\n|\n\r|\r)$")

        # Bool capturing our state regarding allowed mimetypes/extensions
        eol_allowed = False

        # Do EOL audit!
        process = get_change_diff( changeset, ["-p"] )
        for line_raw in process.stdout:
            # Prepare a cleaned version
            line_clean = line_raw.decode('utf-8', 'replace')

            # Check to see if we have changed the commit we are checking
            commit_change = re.match( re_commit, line_raw )
            if commit_change:
                commit = commit_change.group(1).decode('utf-8')
                continue

            # Check to see if we have changed the file that is being checked
            file_change = re.match( re_filename, line_clean )
            if file_change:
                filename = file_change.group(2)
                eol_violation = False
                eol_allowed = False

                # Check if it's an allowed mimetype
                # First - check with the mimetypes system, to see if it can tell
                guessed_type, _ = mimetypes.guess_type(filename)
                if guessed_type in allowed_mimetypes:
                    eol_allowed = True
                    continue

                # Second check: by file extension
                # NOTE: This uses the FIRST dot as extension
                splitted_filename = filename.split(os.extsep)
                # Check if there's an extension or not
                # NOTE This assumes that files use dots for extensions only!
                if len(splitted_filename) > 1:
                    extension = splitted_filename[1]
                    if extension in allowed_extensions:
                        eol_allowed = True

                continue

            # Unless they added it, ignore it
            if not line_clean.startswith("+"):
                continue

            # Check to see if we have an EOL violation
            if re.search( blocked_eol, line_clean ) and not eol_violation:
                # Is this an allowed filename?
                if eol_allowed:
                    continue

                # Failure has been found... handle it
                eol_violation = True
                self.log_failure(commit, "End of Line Style (non-Unix): " + filename);

    # Audit the new, renamed and changed files in the given changesets commits for compliance with the rules in the provided configuration file
    def audit_filename(self, changeset, blocked_rules_file):
        # Initialize
        filename_limits = []

        # Start processing the rules file...
        with open(blocked_rules_file) as configuration:
            for line in configuration:
                regex = line.strip()

                # Skip comments and blank lines
                if regex.startswith("#") or not regex:
                    continue

                # Make sure we have a valid regex
                restriction = re.compile(regex)
                if restriction:
                    filename_limits.append( restriction )

        # Now we can review each commit in turn
        for commit in changeset.commits.values():
            # Check each file that was changed in that commit...
            for filename in commit.files_changed:
                # First, if we didn't create, rename or copy to the file then we shouldn't raise an issue here...
                if commit.files_changed[ filename ]["change"] not in ["A","R","C"]:
                    continue

                # Check to see if this file matches any of the blacklist rules we loaded earlier
                for restriction in filename_limits:
                    if re.search(restriction, filename):
                        self.log_failure(commit.sha1, "Invalid filename: " + filename)

    # Audit the author names of all of the new commits in the provided changeset
    # Names that do not appear to be full names will be rejected, unless they are listed as an exception
    # This can be used to allow GitHub pull requests (which are merged by 'GitHub') to bypass the audit check when needed
    # 
    # Names which do not have a first name and a surname are extremely uncommon and when present are therefore generally invalid. 
    # As we want people to use their actual name when committing we do some checks to make sure that what looks like an actual name is present.
    def audit_names_in_metadata(self, changeset, exceptions):
        # Iterate over commits....
        for commit in changeset.commits.values():
            for name in [ commit.committer_name, commit.author_name ]:
                # Do we have a GitHub Pull Request merge commit, and is it in our list of exceptions?
                # We treat GitHub differently as we want to warn the pusher about this, even if an exception has been given
                if name == 'GitHub' and name in exceptions:
                    self.log_warning(commit.sha1, "Commit has username 'GitHub' (web merge of PR); allowing anyway")
                    continue

                # Is the name in our list of exceptions?
                if name in exceptions:
                    continue

                # Check to see if the name contains spaces - if not - it is probably misconfigured....
                if " " not in name.strip():
                    self.log_failure(commit.sha1, "Non-full name: " + name)
                    continue

    # Audit the author email addresses for all new commits in the provided changesets
    # 
    # Invalid hostnames such as localhost or (none) will be caught by this auditor. 
    # This will ensure that invalid email addresses or users will not show up in commits
    def audit_emails_in_metadata(self, changeset, blocked_addresses, blocked_domains):
        # Setup our list of local domains
        # localhost and (none) are forbidden as they're not valid email addresses
        local_domains = ["localhost", "localhost.localdomain", "(none)"]

        # Iterate over commits....
        for commit in changeset.commits.values():
            for email_address in [ commit.committer_email, commit.author_email ]:
                # Extract the email address, and reject them if extraction fails....
                extraction = re.match("^(\S+)@(\S+)$", email_address)
                if not extraction:
                    self.log_failure(commit.sha1, "Seemingly invalid email address: " + email_address)
                    continue

                # Don't allow addressed which are disallowed...
                if email_address in blocked_addresses:
                    self.log_failure(commit.sha1, "Email address has been forbidden: " + email_address)
                    continue

                # Don't allow domains which are disallowed...
                domain = extraction.group(2)
                if domain in local_domains or domain in blocked_domains:
                    self.log_failure(commit.sha1, "Email address using a blocked domain: " + email_address)
                    continue

                # Ensure they have a valid MX/A entry in DNS....
                try:
                    dns.resolver.query(domain, "MX")
                except (dns.resolver.NoAnswer, dns.exception.Timeout, dns.name.EmptyLabel):
                    try:
                        dns.resolver.query(domain, "A")
                    except (dns.resolver.NoAnswer, dns.exception.Timeout, dns.name.EmptyLabel, dns.resolver.NXDOMAIN):
                        self.log_failure(commit.sha1, "Email address has an invalid domain : " + email_address)
                except (dns.resolver.NXDOMAIN, dns.resolver.NoNameservers):
                    self.log_failure(commit.sha1, "Email address has an invalid domain : " + email_address)

class ChangesetEmailNotifier:
    "Contains infrastructure needed to send summary notifications for a given changeset"

    def notify_summary_email(self, changeset, notification_address):
        # Prepare to begin the assembly of our message body
        summary = [
            "Git repository change summary for {0}".format( changeset.repository.path ),
            "Pushed by {0} into {1} '{2}'.".format( changeset.repository.pusher_username, changeset.ref_type, changeset.ref_name ),
            "Changed from {0} to {1}".format( changeset.old_sha1, changeset.new_sha1 ),
            "Acknowledgement was received that this change introduces only existing code that has been pushed to another public open source repository.",
            "",
            "This change contains the following new commits:"
        ]

        # Now start going over each commit and summarise that...
        for commit_sha1 in changeset.commits:
            # Retrieve the data on the commit
            commit = changeset.commits[ commit_sha1 ]

            # Generate our summary..
            # Generate a human readable time and date for this commit...
            committed_on = commit.datetime.strftime("%d/%m/%Y at %H:%M.")

            # Then determine what the headline for this commit should be...
            headline = "Git commit {0} by {1} on {2}.".format( commit.sha1, commit.committer_name, committed_on )
            if commit.author_name != commit.committer_name:
                headline = "Git commit {0} by {1} (on behalf of {2}) on {3}.".format( commit.sha1, commit.committer_name, commit.author_name, committed_on )

            # Add the items to the body we're assembling
            summary += ['', headline, commit.message.strip(), commit.url]

        # Finalise the body for use
        body = '\n'.join( summary ) + '\n'

        # Now we generate a subject for our email
        subject = "[{0}]: Summary of bulk changes made".format( changeset.repository.path )

        # Handle the normal mailing list mails....
        message = MIMEText( body.encode("utf-8"), 'plain', 'utf-8' )
        message['Subject']  = Header( subject, 'utf-8', 76, 'Subject' )
        message['From']     = self.address_header( "KDE Git Services - Bulk Change", "null@kde.org" )
        message['To']       = Header( notification_address )
        message['X-Commit-Ref']         = Header( changeset.ref_name )
        message['X-Commit-Project']     = Header( changeset.repository.path )
        message['X-Commit-Pusher']      = Header( changeset.repository.pusher_username )

        # Connect to the SMTP Server
        smtp_server = smtplib.SMTP()
        smtp_server.connect()
        smtp_server.sendmail("null@kde.org", notification_address, message.as_string())
        smtp_server.quit()

    def address_header(self, name, email):
        """Helper function to construct an address header for emails - as Python stuffs it up"""
        fixed_name = Header( name ).encode()
        return "{0} <{1}>".format(fixed_name, email)

class CommitEmailNotifier:
    "Contains items needed to send notifications for commits"

    def __init__(self, commit, checker = None, include_url = True):
        # Save our parameters for later...
        self.commit = commit
        self.checker = checker
        self.include_url = include_url

        # Initialize variables we'll need later
        self.keywords = defaultdict(list)

        # Extract the changeset and repository for easier reference later
        self.changeset = commit.changeset
        self.repository = commit.changeset.repository

        # Generate directories affected by the commit
        commit_directories = [os.path.dirname(filename) for filename in commit.files_changed]
        self.commit_directories = list( set(commit_directories) )

        # Connect to the SMTP Server
        self.smtp = smtplib.SMTP()
        self.smtp.connect()

    def __del__(self):
        self.smtp.quit()

    def notify_email(self, notification_address, diff):
        # Build a list of addresses to Cc,
        cc_addresses = self.keywords['email_cc'] + self.keywords['email_cc2']

        # Add the committer to the Cc in case problems have been found
        if self.checker and (self.checker.license_problem or self.checker.commit_problem):
            cc_addresses.append( self.commit.committer_email )

        # Add Sysadmin if infrastructure problems have been found
        if self.checker and self.checker.infra_problem:
            cc_addresses.append( 'sysadmin@kde.org' )

        if self.keywords['email_gui']:
            cc_addresses.append( 'kde-doc-english@kde.org' )

        i18nInfraChange = re.compile("(.*)Messages.sh$")
        if any(i18nInfraChange.match(filename) for filename in self.commit.files_changed):
            cc_addresses.append( 'i18n-infrastructure@kde.org' )

        docbookChange = re.compile("(.*)\.docbook$")
        if any(docbookChange.match(filename) for filename in self.commit.files_changed):
            cc_addresses.append( 'kde-doc-english@kde.org' )

        body = self.body
        if diff and len(diff) < 8000:
            body += "\n" + ''.join(diff)

        # Handle the normal mailing list mails....
        message = MIMEText( body.encode("utf-8", 'replace'), 'plain', 'utf-8' )
        message['Subject']  = Header( self.subject, 'utf-8', 76, 'Subject' )
        message['From']     = self.address_header( self.commit.committer_name, "null@kde.org" )
        message['To']       = Header( notification_address )
        message['Reply-To'] = self.address_header( self.commit.committer_name, self.commit.committer_email )
        if cc_addresses:
            message['Cc']   = Header( ','.join(cc_addresses) )
        message['X-Commit-Ref']         = Header( self.changeset.ref_name )
        message['X-Commit-Project']     = Header( self.repository.path )
        message['X-Commit-Folders']     = Header( ' '.join(self.commit_directories) )
        message['X-Commit-Pusher']      = Header( self.repository.pusher_username )

        # Send email...
        to_addresses = cc_addresses + [notification_address]
        self.smtp.sendmail("null@kde.org", to_addresses, message.as_string())

    def notify_bugzilla(self):
        commit_regex = re.compile("^\s*((CC)?BUGS?|FEATURE)[:=](.+)\n", re.MULTILINE)
        bugs_changed = self.keywords['bug_fixed'] + self.keywords['bug_cc']
        for bug in bugs_changed:
            # Prepare the customised Bugzilla comment
            related_bugs = ["bug " + entry for entry in bugs_changed if entry != bug]
            commit_msg = self.body
            if related_bugs:
                commit_msg = re.sub(commit_regex, "Related: " + ', '.join(related_bugs) + "\n", commit_msg, 1)
            commit_msg = re.sub(commit_regex, "", commit_msg)

            # Prepare the Bugzilla specific message body portion...
            bug_body = []
            bug_body.append( "@bug_id = " + bug )
            if bug in self.keywords['bug_fixed']:
                bug_body.append( "@bug_status = RESOLVED" )
                bug_body.append( "@resolution = FIXED" )
                bug_body.append( "@cf_commitlink = " + self.commit.url )
                if self.keywords['fixed_in']:
                    bug_body.append("@cf_versionfixedin = " + self.keywords['fixed_in'][0])
            bug_body.append( '' )
            bug_body.append( commit_msg )

            body = '\n'.join( bug_body )
            message = MIMEText( body.encode("utf-8"), 'plain', 'utf-8' )
            message['Subject'] = Header( self.subject, 'utf-8', 76, 'Subject' )
            message['From']    = Header( self.commit.committer_email )
            message['To']      = Header( "bugs@phoeni.kde.org" )
            self.smtp.sendmail("null@kde.org", ["bugs@phoeni.kde.org"],
                               message.as_string())

            if bug in self.keywords['bug_fixed']:
                print("Closing bug " + bug)
            else:
                print("Posting comment to bug " + bug)

    def address_header(self, name, email):
        """Helper function to construct an address header for emails - as Python stuffs it up"""
        fixed_name = Header( name ).encode()
        return "{0} <{1}>".format(fixed_name, email)

    @property
    def subject(self):
        # Start by determining the most common path changed by this commit
        # First, did we only impact one directory?
        if len(self.commit_directories) == 1:
            lowest_common_path = self.commit_directories[0]
        else:
            # This works on path segments rather than char-by-char as os.path.commonprefix does
            # and hence avoids problems when multiple directories at the same level start with
            # the same sequence of characters.
            by_levels = zip( *[p.split(os.path.sep) for p in self.commit_directories] )
            equal = lambda name: all( n == name[0] for n in name[1:] )
            lowest_common_path = os.path.sep.join(x[0] for x in itertools.takewhile( equal, by_levels ))

        # If we didn't manage to find the most common path, assume it was the root of the repository
        if not lowest_common_path:
            lowest_common_path = '/'

        # Start constructing the email subject
        # If the branch isn't the default (assumed to be 'master' for now) then include that in the subject too
        repo_path = self.commit.changeset.repository.path
        if self.commit.changeset.ref_name != "master":
            repo_path += "/" + self.commit.changeset.ref_name
        short_msg = self.commit.message.splitlines()[0]
        subject = "[{0}] {1}: {2}".format(repo_path, lowest_common_path, short_msg)

        # Include a silent note in the subject if needed
        if self.keywords['silent']:
            subject += ' (silent)'

        return subject

    @property
    def body(self):
        # Start building up the email body
        # First we want to neatly summarise the commit in question we are emailing about
        # This message should be different if the author and committer of the commit are different...
        if self.commit.author_name != self.commit.committer_name or self.commit.author_email != self.commit.committer_email:
            intro = "Git commit {0} by {1}, on behalf of {2}.".format( self.commit.sha1, self.commit.committer_name, self.commit.author_name )
        else:
            intro = "Git commit {0} by {1}.".format( self.commit.sha1, self.commit.committer_name )

        # Nicely format the time the commit was made on
        committed_on = self.commit.datetime.strftime("Committed on %d/%m/%Y at %H:%M.")

        # And detail for the record who pushed this, and into which branch/tag it went
        pushed_by = "Pushed by {0} into {1} '{2}'."
        pushed_by = pushed_by.format( self.repository.pusher_username, self.changeset.ref_type, self.changeset.ref_name)

        # Now put together the summary
        summary = [intro, committed_on, pushed_by, '', self.commit.message.strip(), '']

        # Now go over all of the files that were affected one by one and summarise everything
        for filename, info in self.commit.files_changed.items():

            # Start with the file name, type of change (add/modify/remove) and a diffstat reporting the number of lines added/removed
            temporary = "{0:<2} +{1:<4} -{2:<4}".format(info["change"], info["added"], info["removed"])
            data = [temporary, filename]

            # If the file was moved or copied from elsewhere, report that information too
            if "source" in info.keys():
                temporary = "[from: {0} - {1}% similarity]".format(info["source"], info["similarity"])
                data.append( temporary )

            # Also add any notes from the checker if we have one
            if self.checker:
                data.extend( self.checker.commit_notes[filename] )

            # Generate the summary line for this file
            summary.append( ' '.join(data) )

        # Did the checker pick up any licensing issues?
        # If so add the appropriate note explaining that
        if self.checker and self.checker.license_problem:
            summary.append(
                "\nThe files marked with a * at the end have a non valid license. "
                "Please read: https://community.kde.org/Policies/Licensing_Policy and use the headers which are listed at that page.\n"
            )

        # Otherwise did the checker pick up any commit content problems?
        if self.checker and self.checker.commit_problem:
            summary.append( 
                "\nThe files marked with ** at the end have a problem. Either the file contains a trailing space or the file contains a call to "
                "potentially dangerous code. Please read: https://community.kde.org/Sysadmin/CommitHooks#Email_notifications for further information. "
                "Please either fix the trailing space or review the dangerous code.\n"
            )

        # Finally add a link to the commit url
        if self.include_url:
            summary.append( "\n" + self.commit.url )

        # Then finalise the email body
        return '\n'.join( summary ) + '\n'

    def determine_keywords(self):
        """Parse special keywords in commits to determine further post-commit actions."""

        split = {}
        split['email_cc']  = re.compile("^\s*CC[-_]?MAIL[:=]\s*(.*)")
        split['email_cc2'] = re.compile("^\s*C[Cc][:=]\s*(.*)")
        split['fixed_in']  = re.compile("^\s*FIXED[-_]?IN[:=]\s*(.*)")

        numeric = {}
        numeric['bug_fixed'] = re.compile("^\s*(?:BUGS?|FEATURE)[:=]\s*(.+)")
        numeric['bug_cc']    = re.compile("^\s*CCBUGS?[:=]\s*(.+)")

        presence = {}
        presence['email_gui'] = re.compile("^\s*GUI:")
        presence['silent']    = re.compile("(?:CVS|SVN|GIT|SCM).?SILENT")
        presence['notes']     = re.compile("(?:Notes added by 'git notes add'|Notes removed by 'git notes remove')")

        results = defaultdict(list)
        for line in self.commit.message.split("\n"):
            # If our line starts with Summary: (as it does when using Arcanist's default template) then strip this off
            # This allows for people to fill keywords in the Differential Summary and have this work smoothly for them
            line = re.sub("^Summary: (.+)", "\g<1>", line)

            # Start processing our keywords...
            for (name, regex) in split.items():
                match = re.match( regex, line )
                if match:
                    results[name] += [result.strip() for result in match.group(1).split(",")]

            for (name, regex) in numeric.items():
                match = re.match( regex, line )
                if match:
                    results[name] += re.findall("(\d{1,10})", match.group(1))

            for (name, regex) in presence.items():
                if re.match( regex, line ):
                    results[name] = True

        self.keywords = results

class CiaNotifier:
    "Notifies CIA of changes to a repository"

    MESSAGE = E.message
    GENERATOR = E.generator
    SOURCE = E.source
    TIMESTAMP = E.timestamp
    BODY = E.body
    COMMIT = E.commit

    def __init__(self):

        # Generate the non-variant part of the XML message sent to CIA.
        name = E.name("KDE CIA Python client")
        version = E.version("1.00")
        url = E.url("http://projects.kde.org/repo-management")
        self._generator = self.GENERATOR(name, version, url)

        self.smtp = smtplib.SMTP()
        self.smtp.connect()

    def __del__(self):
        self.smtp.quit()

    def notify(self, commit):

        """Send the commmit notification to CIA.

        The message is created incrementally using lxml's "E" builder."""

        # Build the <files> section for the template...
        files = E.files()

        commit_msg = commit.message.strip()
        commit_msg = re.sub(r'[\x00-\x09\x0B-\x1f\x7f-\xff]', '', commit_msg)

        for filename in commit.files_changed:
            safe_filename = re.sub(r'[\x00-\x09\x0B-\x1f\x7f-\xff]', '', filename)
            file_element = E.file(safe_filename)
            files.append(file_element)

        # Build the message
        cia_message = self.MESSAGE()
        cia_message.append(self._generator)

        source = self.SOURCE(E.project("KDE"))
        source.append(E.module(commit.changeset.repository.path))
        source.append(E.branch(commit.changeset.ref_name))

        cia_message.append(source)
        cia_message.append(self.TIMESTAMP(commit.date))

        body = self.BODY()

        commit_data = self.COMMIT()
        commit_data.append(E.author(commit.author_name))
        commit_data.append(E.revision(commit.description))
        commit_data.append(files)
        commit_data.append(E.log(commit_msg))
        commit_data.append(E.url(commit.url))

        body.append(commit_data)
        cia_message.append(body)

        # Convert to a string
        commit_xml = etree.tostring(cia_message)

        # Craft the email....
        message = MIMEText( commit_xml, 'xml', 'utf-8' )
        message['Subject'] = "DeliverXML"
        message['From'] = "sysadmin@kde.org"
        message['To'] = "commits@platna.kde.org"

        # Send email...
        self.smtp.sendmail("sysadmin@kde.org", ["commits@platna.kde.org"], message.as_string())

class CommitChecker:

    """Checker class for commit information such as licenses, or potentially
    unsafe practices."""

    @property
    def license_problem(self):
        return self._license_problem

    @property
    def commit_problem(self):
        return self._commit_problem

    @property
    def infra_problem(self):
        return self._infra_problem

    @property
    def commit_notes(self):
        return self._commit_notes

    def check_commit_license_traditionalheader(self, filename, text):
        problemfile = False
        gl = qte = license = wrong = ""
        text = re.sub("^\#", "", text)
        text = re.sub("\t\n\r", "   ", text)
        text = re.sub("[^ A-Za-z.@0-9]", "", text)
        text = re.sub("\s+", " ", text)

        if re.search("version 2(?:\.0)? .{0,40}as published by the Free Software Foundation", text):
            gl = " (v2)"

        if re.search("version 2(?:\.0)? of the License", text):
            gl = " (v2)"

        if re.search("version 3(?:\.0)? .{0,40}as published by the Free Software Foundation", text):
            gl = " (v3)"

        if re.search("either version 2(?: of the License)? or at your option any later version", text):
            gl = " (v2+)"

        if re.search("version 2(?: of the License)? or at your option version 3", text):
            gl = " (v2/3)"

        if re.search("version 2(?: of the License)? or at your option version 3 or at the discretion of KDE e.V.{10,60}any later version", text):
            gl = " (v2/3+eV)"

        if re.search("either version 3(?: of the License)? or at your option any later version", text):
            gl = " (v3+)"

        if re.search("version 2\.1 as published by the Free Software Foundation", text):
            gl = " (v2.1)"

        if re.search("2\.1 available at: http:\/\/www.fsf.org\/copyleft\/lesser.html", text):
            gl = " (v2.1)"

        if re.search("either version 2\.1 of the License or at your option any later version", text):
            gl = " (v2.1+)"

        if re.search("([Pp]ermission is given|[pP]ermission is also granted|[pP]ermission) to link (the code of )?this program with (any edition of )?(Qt|the Qt library)", text):
            qte = " (+Qt exception)"

        # Check for an old FSF address
        # MIT licenses will trigger the check too, as "675 Mass Ave" is MIT's address
        if re.search("(?:675 Mass Ave|59 Temple Place|Suite 330|51 Franklin Steet|02139|02111-1307)", text, re.IGNORECASE):
            # "51 Franklin Street, Fifth Floor, Boston, MA 02110-1301" is the right FSF address
            wrong = " (wrong address)"
            problemfile = True

        # traditional license header LGPL or GPL
        if re.search("under (the (terms|conditions) of )?the GNU (Library|Lesser) General Public License", text):
            license = "LGPL" + gl + wrong + " " + license

        if re.search("under (the (terms|conditions) of )?the (Library|Lesser) GNU General Public License", text):
            license = "LGPL" + gl + wrong + " " + license

        if re.search("under (the (terms|conditions) of )?the (GNU )?LGPL", text):
            license = "LGPL" + gl + wrong + " " + license

        if re.search("[Tt]he LGPL as published by the Free Software Foundation", text):
            license = "LGPL" + gl + wrong + " " + license

        if re.search("LGPL with the following explicit clarification", text):
            license = "LGPL" + gl + wrong + " " + license

        if re.search("under (the terms of )?(version 2 of )?the GNU (General Public License|GENERAL PUBLIC LICENSE)", text):
            license = "GPL" + gl + qte + wrong + " " + license

        # QPL
        if re.search("may be distributed under the terms of the Q Public License as defined by Trolltech AS", text):
            license = "QPL " + license

        # X11, BSD-like
        if re.search("Permission is hereby granted free of charge to any person obtaining a copy of this software and associated documentation files", text):
            license = "X11 (BSD like) " + license

        # MIT license
        if re.search("Permission to use copy modify (and )?distribute(and sell)? this software and its documentation for any purpose", text):
            license = "MIT " + license

        # BSD
        if re.search("MERCHANTABILITY( AND|| or) FITNESS FOR A PARTICULAR PURPOSE", text) and not re.search("GPL", license):
            license = "BSD " + license

        # MPL
        if re.search("subject to the Mozilla Public License Version 1.1", text):
            license = "MPL 1.1 " + license

        if re.search("Mozilla Public License Version 1\.0/", text):
            license = "MPL 1.0 " + license

        # Artistic license
        if re.search("under the Artistic License", text):
            license = "Artistic " + license

        # Public domain
        if re.search("Public Domain", text, re.IGNORECASE) or re.search(" disclaims [Cc]opyright", text):
            license = "Public Domain " + license

        # Auto-generated
        if re.search("(All changes made in this file will be lost|This file is automatically generated|DO NOT EDIT|DO NOT delete this file|[Gg]enerated by|uicgenerated|produced by gperf)", text):
            license = "GENERATED FILE"
            problemfile = True

        # Don't bother with trivial files.
        if not license and len(text) < 128:
            license = "Trivial file"

        # About every license has this clause; but we've failed to detect which type it is.
        if not license and re.search("This (software|package)( is free software and)? is provided ",
                                     text, re.IGNORECASE):
            license = "Unknown license"
            problemfile = True

        # Either a missing or an unsupported license
        if not license:
            license = "UNKNOWN"
            problemfile = True

        license = license.strip()

        return problemfile, license

    def check_commit_license_spdx(self, filename, text):
        problemfile = False
        license = ""

        # get SPDX expression statement
        regex = re.search("SPDX-License-Identifier: (.*)", text)
        spdxExpression = ""
        if not regex == None:
            spdxExpression = regex.group(1)

        # match any SPDX expressions that are split by "OR"
        # this is a simpler solution than implementing a full BNF grammar
        for expression in re.split("\sOR\s", spdxExpression):

            # SPDX based LGPL or GPL
            if expression == "LGPL-2.0-only":
                license = "LGPL(v2.0) " + license + " "
            if expression == "LGPL-2.0-or-later":
                license = "LGPL(v2.0+) " + license + " "
            if expression == "LGPL-2.1-only":
                license = "LGPL(v2.1) " + license + " "
            if expression == "LGPL-2.1-or-later":
                license = "LGPL(v2.1+) " + license + " "
            if expression == "LGPL-3.0-only":
                license = "LGPL(v3.0) " + license + " "
            if expression == "LGPL-3.0-or-later":
                license = "LGPL(v3.0+) " + license + " "
            if expression == "LicenseRef-KDE-Accepted-LGPL":
                license = "LGPL(3+eV) " + license + " "
            if expression == "GPL-2.0-only":
                license = "GPL(v2.0) " + license + " "
            if expression == "GPL-2.0-or-later":
                license = "GPL(v2.0+) " + license + " "
            if expression == "GPL-3.0-only":
                license = "GPL(v3.0) " + license + " "
            if expression == "GPL-3.0-or-later":
                license = "GPL(v3.0+) " + license + " "
            if expression == "LicenseRef-KDE-Accepted-GPL":
                license = "GPL(3+eV) " + license + " "

            # Non-GPL/LGPL
            if expression == "QPL-1.0":
                license = "QPL " + license + " "
            if expression == "X11":
                license = "X11 " + license + " "
            if expression == "MIT":
                license = "MIT " + license + " "
            if expression == "BSD-2-Clause" or expression == "BSD-3-Clause":
                license = "BSD " + license + " "
            if expression == "MPL-1.1":
                license = "MPL 1.1 " + license + " "
            if expression == "MPL-1.0":
                license = "MPL 1.0 " + license + " "
            if expression == "Artistic-1.0":
                license = "Artistic 1.0 " + license + " "
            if expression == "Artistic-2.0":
                license = "Artistic 2.0 " + license + " "

        # Either a missing or an unsupported license
        if not license:
            license = "UNKNOWN"
            problemfile = True

        license = license.strip()

        return problemfile, license

    def check_commit_license(self, filename, text):
        licenseproblem_spdx, license_spdx = self.check_commit_license_spdx(filename, text)
        licenseproblem_header, license_header = self.check_commit_license_traditionalheader(filename, text)

        # take precedence for SPDX based license statements
        if not licenseproblem_spdx:
            self._commit_notes[filename].append( (" "*4) + "[License: " + license_spdx + "]")
        elif licenseproblem_spdx and not licenseproblem_header:
            self._commit_notes[filename].append( (" "*4) + "[License: " + license_header + "]")
        else:
            # Don't bother with trivial files.
            if len(text) < 128:
                license = "Trivial file"
                self._commit_notes[filename].append( (" "*4) + "[License: " + license + "]")
            else:
                self._license_problem = True
                self._commit_notes[filename].append( " *")

    def check_commit_problems(self, commit, diff):

        """Check for potential problems in a commit."""

        # Initialise
        self._license_problem = False
        self._infra_problem = False
        self._commit_problem = False
        self._commit_notes = defaultdict(list)

        # Unsafe regex checks...
        unsafe_matches = []
        unsafe_matches.append( r"\b(KRun::runCommand|K3?ShellProcess|setUseShell|setShellCommand)\b\s*[\(\r\n]" )
        unsafe_matches.append( r"\b(system|popen|mktemp|mkstemp|tmpnam|gets|syslog|strptime)\b\s*[\(\r\n]" )
        unsafe_matches.append( r"(scanf)\b\s*[\(\r\n]" )
        valid_filename_regex = r"\.(cpp|cc|cxx|C|c\+\+|c|l|y||h|H|hh|hxx|hpp|h\+\+|qml)$"

        # Retrieve the diff and do the problem checks...
        filename = ""
        filediff = []
        for line in diff:
            file_change = re.match( "^diff --(cc |git a\/.+ b\/)(.+)$", line )
            if file_change:
                # Are we changing file? If so, we have the full diff, so do a license check....
                if filename != "" and commit.files_changed[ filename ]["change"] in ['A'] and re.search(valid_filename_regex, filename):
                    self.check_commit_license(filename, ''.join(filediff))

                filediff = []
                filename = file_change.group(2)
                continue

            # Diff headers are bogus
            if re.match("@@ -\d+,\d+ \+\d+ @@", line):
                filediff = []
                continue

            # Do an incremental check for *.desktop syntax errors....
            if re.search("\.desktop$", filename) and re.search("[^=]+=.*[ \t]$", line) and line.startswith("+") and not re.match("^\+#", line):
                self._commit_notes[filename].append( "[TRAILING SPACE] **" )
                self._commit_problem = True

            # Check for things which are unsafe...
            for safety_match in unsafe_matches:
                match = re.match(safety_match, line)
                if match:
                    note = "[POSSIBLY UNSAFE: {0}] **".format( match.group(1) )
                    self._commit_notes[filename].append(note)
                    self._commit_problem = True

            # Check for references to KDE.org infrastructure which are being added without permission
            if re.search(".*(download|files)\.kde\.org.*", line) and line.startswith("+") and not re.match("^(packaging|websites)/.*", commit.changeset.repository.path):
                self._commit_notes[filename].append( "[INFRASTRUCTURE]" )
                self._infra_problem = True

            # Store the diff....
            filediff.append(line)

        if filename != "" and commit.files_changed[ filename ]["change"] in ['A'] and re.search(valid_filename_regex, filename):
            self.check_commit_license(filename, ''.join(filediff))

def read_command( *command, shell=False ):
    process = subprocess.Popen(command, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return process.stdout.readline().strip().decode('utf-8')

def get_change_diff( changeset, log_arguments ):
    # Prepare to run....
    command = ["git", "show", "--pretty=format:%xff%H%xff", "--stdin", "-C"]
    command.extend(log_arguments)
    # Run the command!
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # Start passing through the list of commits to Git
    for sha1 in changeset.commits:
        process.stdin.write(sha1.encode() + b"\n")
    process.stdin.close()
    return process
