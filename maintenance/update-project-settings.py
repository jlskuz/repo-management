#!/usr/bin/python3
import os
import re
import sys
import yaml
import argparse
import gitlab

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Updates the Gitlab configuration based on repo-metadata')
parser.add_argument('--metadata-path', help='Path to the metadata to check', required=True)
parser.add_argument('--instance', help='Gitlab instance to work with', required=True)
parser.add_argument('--token', help='User token to use when authenticating with Gitlab', required=True)
parser.add_argument('--webhook-rules', help='Path to YAML format file detailing the webhooks to be registered', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.metadata_path ):
    print("Unable to locate specified metadata location: {}".format(args.metadata_path))
    sys.exit(1)

# Make sure the webhook rules file exists as well
if not os.path.exists( args.webhook_rules ):
    print("Unable to locate the specified webhook rules configuration: {}".format(args.webhook_rules))
    sys.exit(1)

# Connect to Gitlab
gitlabServer = gitlab.Gitlab( args.instance, private_token=args.token )

# The following are the patterns we want to protect
branchPatternsToProtect = ['master', 'kf5', 'kf6', '*.*']

# This is the template we would like to use for squash commits
squashCommitTemplate = "%{title}\n\n%{description}"

# Read in the webhook rules configuration
webhookRules = yaml.safe_load( open(args.webhook_rules, 'r') )

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Do we have a metadata.yaml file?
    if 'metadata.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Are we looking at a neon/ path?
    regexToMatch = os.path.join( args.metadata_path, 'neon', '.*' )
    if re.match( regexToMatch, currentPath ):
        # Then we are also not interested
        # These repositories are looked after by a separate script (update-neon-project-settings.py) which have different sets of rules around them
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'metadata.yaml' )
    metadataFile = open( metadataPath, 'r' )
    metadata = yaml.safe_load( metadataFile )

    # create gitlab project instance
    project = gitlabServer.projects.get(metadata['repopath'])

    # We do this in two rounds, first round is settings which should not fail
    # second round is cosmetic data such as name and description.

    # First we update the merge method setting
    project.merge_method = 'ff'
    # As well as the only allow merge if discussions are resolved setting
    project.only_allow_merge_if_all_discussions_are_resolved = True
    # Ensure we cleanup branches on source repositories too
    project.remove_source_branch_after_merge = True
    # Disable request access option for all projects, as we want access control managed by developer account process
    project.request_access_enabled = False
    # Disable various bits of functionality in Gitlab which we don't use and is just distracting
    project.releases_access_level = 'disabled'
    project.environments_access_level = 'disabled'
    #project.model_experiments_access_level = 'disabled'
    project.feature_flags_access_level = 'disabled'
    project.infrastructure_access_level = 'disabled'
    project.monitor_access_level = 'disabled'
    # Disable other bits of functionality we don't use
    project.service_desk_enabled = False
    # Enable CI for the project
    project.builds_access_level = 'enabled'
    # Make sure that we don't keep old build artifacts though
    project.keep_latest_artifact = False
    # Set the squash commit template too
    project.squash_commit_template = squashCommitTemplate

    # Save the first round of information
    project.save()

    # Now second round which is cosmetics.
    # check sanity of the description and make sure that it is less then 250
    if metadata['description'] is not None and (len(metadata['description']) > 250 or len(metadata['description']) == 0):
        print("Description is too long or empty for " + metadata['repopath'])
    else:
        project.description = metadata['description']

    # Set name and description
    project.name = metadata['name']

    # Finally save the settings
    try:
        project.save()
    except:
        print ("Error saving project " + metadata['repopath'])

    # Next up we propagate information on Bugzilla to Gitlab
    # We only do this if the information is available in the metadata
    if 'bugzilla' in metadata:
        # Retrieve the necessary service and set it up...
        # We assume to begin with that we don't have a component...
        bugzillaService = project.services.get('bugzilla', lazy=True)
        bugzillaService.issues_url = 'https://bugs.kde.org/:id'
        bugzillaService.new_issue_url = "https://bugs.kde.org/enter_bug.cgi?product=%s" % ( metadata['bugzilla']['product'] )
        bugzillaService.project_url = "https://bugs.kde.org/buglist.cgi?product=%s&resolution=---" % ( metadata['bugzilla']['product'] )

        # Then we check to see if we do have a component
        if 'component' in metadata['bugzilla']:
            bugzillaService.new_issue_url = "https://bugs.kde.org/enter_bug.cgi?product=%s&component=%s" % ( metadata['bugzilla']['product'], metadata['bugzilla']['component'] )
            bugzillaService.project_url = "https://bugs.kde.org/buglist.cgi?product=%s&component=%s&resolution=---" % ( metadata['bugzilla']['product'], metadata['bugzilla']['component'] )

        # Commit it to Gitlab!
        bugzillaService.save()

    # Now lets make sure the webhooks are configured appropriately...
    # Start by going over the list of rules we have to see what matches we have
    expectedWebhooks = {}
    for rule in webhookRules:
        # Does this rule match?
        if not re.match( rule['projects'], metadata['repopath'] ):
            continue

        # We have a match!
        # Determine what the settings should be for this webhook
        # Start by setting some sensible defaults
        webhookSettings = {
            "push_events": False,
            "push_events_branch_filter": "",
            "issues_events": False,
            "confidential_issues_events": False,
            "merge_requests_events": False,
            "tag_push_events": False,
            "note_events": False,
            "confidential_note_events": False,
            "job_events": False,
            "pipeline_events": False,
            "wiki_page_events": False,
            "deployment_events": False,
            "releases_events": False,
            "enable_ssl_verification": False,
        }

        # Now apply the settings specified from this webhook
        webhookSettings.update( rule )
        # Make sure the 'projects' entry is not retained
        del webhookSettings['projects']
        # Add it to the list of hooks we are expecting to find
        expectedWebhooks[ webhookSettings['url'] ] = webhookSettings

    # Do we need to update or remove any hooks?
    for webhook in project.hooks.list():
        # Is this webhook in the list of of hooks we are expecting to find?
        if webhook.url not in expectedWebhooks:
            # Remove it
            print("Deleting webhook on " + project.path_with_namespace)
            webhook.delete()
            continue

        # Update it's settings
        for key, value in expectedWebhooks[ webhook.url ].items():
            if key not in webhook.attributes or webhook.attributes[ key ] != value:
                setattr( webhook, key, value )

        # Commit the update
        webhook.save()
        # Remove it from the list of hooks we expect to see
        del expectedWebhooks[ webhook.url ]

    # Add any remaining webhooks
    for hook in expectedWebhooks.values():
        # Create this hook
        project.hooks.create( hook )

    # Now we do the repository protection rules....
    # Start a list of known protection rules
    knownRules = []

    # Go over the protection rules we have and make sure they conform with the above
    for protected in project.protectedbranches.list():
        # Is it one of the ones we're allowed to have?
        if protected.name not in branchPatternsToProtect:
            # As it isn't in the list, get rid of it
            protected.delete()
            # Then go on to the next one
            continue

        # Now we need to make sure the access levels are correct
        if protected.merge_access_levels[0]['access_level'] != gitlab.const.DEVELOPER_ACCESS or protected.push_access_levels[0]['access_level'] != gitlab.const.DEVELOPER_ACCESS:
            # Turns out the Gitlab API does not support updating protection rules (at least not with the Python module)
            # We therefore have to remove these ones as well
            protected.delete()
            # Now go on to the next one
            continue

        # Finally add it to the list of rules we've found so we can keep track
        knownRules.append( protected.name )

    # Now determine which ones we are missing
    missingRules = [ pattern for pattern in branchPatternsToProtect if pattern not in knownRules ]

    # Go over the ones we are missing
    for rule in missingRules:
        # And create them
        project.protectedbranches.create({
            'name': rule,
            'merge_access_level': gitlab.const.DEVELOPER_ACCESS,
            'push_access_level': gitlab.const.DEVELOPER_ACCESS
        })

# All done!
sys.exit(0)

