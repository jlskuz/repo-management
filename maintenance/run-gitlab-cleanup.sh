#!/bin/bash

# Remove empty directories - Package Registry
find -L /srv/git/shared/packages -mindepth 3 -type d -empty -delete -print

# Remove empty directories - Job/Pipeline artifacts
find -L /srv/git/shared/artifacts -mindepth 4 -maxdepth 7 -not -path */pipelines -type d -empty -delete -print

# Instruct Gitlab to tidy up all repositories including:
# - Update pool repositories
# - Execute housekeeping on all repositories (which will repack them and de-duplicate their contents via the pool repositories)
cd ~/gitlab/
bundle exec rails runner -e production ~/repo-management/maintenance/gitlab-repository-cleanup.rb

# Now we repack the pool repositories
# Due to the frequent small commits by scripty that do things like just incrementing date/time notations we end up with a very poorly packed pool repositories
cd /srv/git/repositories/@pools/
for repo in `find -maxdepth 3 -mindepth 3 -type d`; do
    cd /srv/git/repositories/@pools/$repo
    git gc --aggressive
    sleep 2
    cd /srv/git/repositories/@pools/;
done
