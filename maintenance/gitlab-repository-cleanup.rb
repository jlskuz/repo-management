all_groups = Group.all

# Refresh the object pool for all our mainline repositories
all_groups.each do |current_group|
    puts current_group.full_path + "\n"
    current_group.projects.each do |current_project|
      puts current_project.full_path + "\n"
      if current_project.pool_repository
        current_project.pool_repository.object_pool.fetch
      end
    end
end; nil

# Gather the list of projects to optimise
# A this should be run once a week, only look at those projects with activity in the last month
last_changed_days_ago = 30
projects_to_housekeep = Project.where( "last_activity_at >= ?", last_changed_days_ago.days.ago )

# Force housekeeping to run on all our projects
projects_to_housekeep.each do |p|
   puts p.full_path + "\n"
   Repositories::HousekeepingService.new(p, :eager).execute
   sleep 3
end

